import { reactive } from 'vue'
export default reactive([
  {
    label: 'Hormone Replacement Therapy',
    month: 5,
    status: 2,
  },
  {
    label: 'Ozone Therapy',
    month: 5,
    status: 2,
  },
  {
    label: 'NAD',
    month: 5,
    status: 2,
  },
  {
    label: 'Hormone Replacement Therapy',
    month: 5,
    status: 2,
  },
  {
    label: 'Ozone Therapy',
    month: 1,
    status: 1,
  },
  {
    label: 'NAD',
    month: 1,
    status: 1,
  },
  {
    label: 'Hormone Replacement Therapy',
    month: 3,
    status: 0,
  },
])
