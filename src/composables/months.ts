import { reactive } from 'vue'
export default reactive([
  {
    label: 'Jan',
    index: 0,
  },
  {
    label: 'Feb',
    index: 1,
  },
  {
    label: 'Mar',
    index: 2,
  },
  {
    label: 'Apr',
    index: 3,
  },
  {
    label: 'May',
    index: 4,
  },
  {
    label: 'Jun',
    index: 5,
  },
  {
    label: 'Jul',
    index: 6,
  },
  {
    label: 'Aug',
    index: 7,
  },
  {
    label: 'Sep',
    index: 8,
  },
  {
    label: 'Oct',
    index: 9,
  },
  {
    label: 'Nov',
    index: 10,
  },
  {
    label: 'Dez',
    index: 11,
  },
])
